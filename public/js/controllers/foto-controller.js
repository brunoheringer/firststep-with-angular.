angular.module('AplicationTest')
    .controller('FotoController', function($scope, recursoFoto, $routeParams) {
        $scope.foto = {};
        $scope.mensagem = '';
        $scope.ExibeId = false;
    
         if($routeParams.fotoId) {
            $scope.ExibeId = true;
            recursoFoto.get({fotoId: $routeParams.fotoId}, function(foto) {
                $scope.foto = foto; 
            }, function(erro) {
                console.log(erro);
                $scope.mensagem = 'Não foi possível obter o dado'
            });
        }

      $scope.submeter = function() {

            if ($scope.formulario.$valid) {
                if($routeParams.fotoId) {
                    recursoFoto.update({fotoId: $scope.foto._id}, 
                        $scope.foto, function() {
                        $scope.mensagem = 'Alterada com sucesso';
                    }, function(erro) {
                        console.log(erro);
                        $scope.mensagem = 'Não foi possível alterar';
                    });

                } else {
                    recursoFoto.save($scope.foto, function() {
                        $scope.foto = {};
                        $scope.mensagem = 'Cadastrado com sucesso';
                    }, function(erro) {
                        console.log(erro);
                        $scope.mensagem = 'Não foi possível cadastrar';
                    });
                }
            }
        };

    });