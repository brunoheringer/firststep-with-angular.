# README #

This project is only a first test with Angular.JS. Follow under more informations about this.

>* I didn't use OO. ~~(no máximo um JSON pra falar que é objeto)~~  
>* The data are ever 
>* The data is always "carried" in Json. Even saved in JSON. (`Data` file in the root of the folder)
>* We used HTML, JavaScript and a mini-BackEnd in Node.JS. Just to provide some http features. (third-party backend)


### How do I get set up? ###   
* **Summary**   
This project uses [Node.JS](https://nodejs.org/en/). The first step will be read the stage **How to run**.   
This project it is using **[Angular1](https://angularjs.org/)**.  

* **Dependencies**  
**[Node.JS](https://nodejs.org/en/)**  
Windows - Baixe o instalador clicando no grande botão install diretamente da página do Node.js. Durante a instalação, você apenas clicará botões para continuar o assistente. Não troque a pasta padrão do Node.js durante a instalação a não ser que você saiba exatamente o que está fazendo.    
**[Visual Studio Core](https://nodejs.org/en/)**  
You can use the famous "next, next, finish"

* **Database configuration**  
This project use [NEDB](https://github.com/louischatriot/nedb)  
A NO-SQL DB. Totally made in Node.js and it doesn't need any special configuration.  
The database stay in root folder of this project with the name `data`.

* **How to run**  
Depois do Node.js ter sido instalado, dentro da pasta do projeto que você descompactou busque todas as dependências do projeto através do seu terminal favorito com o comando. `npm install` e após basta iniciar com o comando `npm start`
> ATENÇÃO se durante o `npm install` aparecer mensagens de erro, procure pelo texto npm ERR! self signed certificate. Se ele existir, isso indica um problema no certificado do seu roteador (proxy). Basta rodar o comando no terminal `npm set strict-ssl false` que resolvera este problema.  (antes do `npm start`)


### Contribution guidelines ###
* Code review / Version: **Alpha | 0**

### Who do I talk to? ###
* Bruno Heringer | 31 98822-2391

### Examples ###
Some photos of this aplication:
Main Screem
![1.png](https://bitbucket.org/repo/BggAneo/images/1713292793-1.png)  
------
Main Screem | Filtering
![2.png](https://bitbucket.org/repo/BggAneo/images/2857137358-2.png)  
------
Create Scremm
![3.png](https://bitbucket.org/repo/BggAneo/images/125291047-3.png)  
------